<?php
// HTTP
define('HTTP_SERVER', 'http://tex/admin/');
define('HTTP_CATALOG', 'http://tex/');

// HTTPS
define('HTTPS_SERVER', 'http://tex/admin/');
define('HTTPS_CATALOG', 'http://tex/');

// DIR
define('DIR_APPLICATION', 'D:\OpenServer/OSPanel/domains/tex/public_html/admin/');
define('DIR_SYSTEM', 'D:\OpenServer/OSPanel/domains/tex/public_html/system/');
define('DIR_IMAGE', 'D:\OpenServer/OSPanel/domains/tex/public_html/image/');
define('DIR_STORAGE', 'D:\OpenServer/OSPanel/domains/tex/storage/');
define('DIR_CATALOG', 'D:\OpenServer/OSPanel/domains/tex/public_html/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'tex');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
define('OPENCARTFORUM_SERVER', 'https://opencartforum.com/');
