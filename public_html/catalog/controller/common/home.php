<?php
class ControllerCommonHome extends Controller {
	public function index() {

		if (isset($this->request->get['route'])) {
			$canonical = $this->url->link('common/home');
			if ($this->config->get('config_seo_pro') && !$this->config->get('config_seopro_addslash')) {
				$canonical = rtrim($canonical, '/');
			}
			$this->document->addLink($canonical, 'canonical');
		}

		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$cart = array_flip(array_column($this->cart->getProducts(), 'product_id'));

		//$results = $this->cache->get('trade_import_common_home');

		if (false) {
			$data['categories'] = $results;
		} else {
			$results = $this->model_catalog_product->getProductsByCategories(array('sort_order' => 'viewed DESC', 'product_limit' => 8, 'return_raw' => true));
		
			$data['categories'] = array();

			foreach ($results as $result) {
				$data['categories'][$result['category_id']]['category_id'] = $result['category_id'];
				$data['categories'][$result['category_id']]['name'] = $result['category_name'];
				$data['categories'][$result['category_id']]['href'] = $this->url->link('product/category', 'path=' . $result['category_id']);

				if ($result['product_image']) {
					$image = $this->model_tool_image->resize($result['product_image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['categories'][$result['category_id']]['products'][$result['product_id']]['product_id'] = $result['product_id'];
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['thumb'] = $image;
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['name'] = $result['product_name'];
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['description'] = utf8_substr(trim(strip_tags(html_entity_decode($result['product_description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..';
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['price_raw'] = $result['special'] ?: $result['price'];
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['price'] = $price;
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['in_cart'] = isset($cart[$result['product_id']]);
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['special'] = $special;
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['special_discount'] = $result['special_discount'];
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['tax'] = $tax;
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['minimum'] = $result['minimum'] > 0 ? $result['minimum'] : 1;
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['rating'] = $result['rating'];
				$data['categories'][$result['category_id']]['products'][$result['product_id']]['href'] = $this->url->link('product/product', 'product_id=' . $result['product_id']);
			}
			//$this->cache->set('trade_import_common_home', $data['categories']);
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}