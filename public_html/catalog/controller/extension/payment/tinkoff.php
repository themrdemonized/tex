<?php

class ControllerExtensionPaymentTinkoff extends Controller
{
    public function index()
    {
        $this->load->language('extension/payment/tinkoff');
        $this->load->model('extension/payment/tinkoff');
        $url = $this->model_extension_payment_tinkoff->getPaymentUrl();
        $data = [
            'status' => 'error',
        ];

        if ($url) {
            $data = [
                'status' => 'success',
                'url' => $url,
            ];
        }

        return $this->load->view('extension/payment/tinkoff', $data);
    }

    public function callback()
    {
        $request = json_decode(file_get_contents("php://input"));
        $request->Success = $request->Success ? 'true' : 'false';
        $request = (array)$request;
        $this->load->model('extension/payment/tinkoff');

        if ($request['Token'] !== $this->model_extension_payment_tinkoff->getToken($request)) {
            die('NOTOK');
        }

        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($request['OrderId']);

        if (!$order) {
            die('NOTOK');
        }

        if ($request['Amount'] < (float)$order['total'] * 100) {
            die('NOTOK');
        }

        switch ($request['Status']) {
            case 'AUTHORIZED':
                $status = $this->config->get('payment_tinkoff_authorized');
                break;
            case 'CONFIRMED':
                $status = $this->config->get('payment_tinkoff_confirmed');
                break;
            case 'CANCELED':
                $status = $this->config->get('payment_tinkoff_cancelled');
                break;
            case 'REJECTED':
                $status = $this->config->get('payment_tinkoff_rejected');
                break;
            case 'REVERSED':
                $status = $this->config->get('payment_tinkoff_reversed');
                break;
            case 'REFUNDED':
                $status = $this->config->get('payment_tinkoff_refunded');
                break;
            default:
                $status = null;
                break;
        }

        if (!$status) {
            die('NOTOK');
        }

        $this->load->model('checkout/order');
        $this->model_checkout_order->addOrderHistory((int)$request['OrderId'], $status);

        die('OK');
    }

    public function failure()
    {
        $this->load->language('extension/payment/tinkoff');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        return $this->response->setOutput($this->load->view('extension/payment/tinkoff_failure', $data));
    }

    public function success()
    {
        $this->load->language('extension/payment/tinkoff');
        $this->load->language('checkout/success');
        
        if ( isset($this->session->data['order_id']) && ( ! empty($this->session->data['order_id']))  ) {
            $this->session->data['last_order_id'] = $this->session->data['order_id'];
        }

        if (isset($this->session->data['order_id'])) {
            $product_data = $this->cart->getProducts();
            if ($this->config->get('module_trade_import_enable_order')) {
                //Connect to Trade and send order
                $time = time();
                $date = date('c', $time);
                $this->load->model('extension/module/trade_import');
                $this->load->model('account/customer');
                $order_data = array();
                if ($this->customer->isLogged()) {
                    $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
                    $order_data['firstname'] = empty($customer_info['firstname']) ? 'null' : $customer_info['firstname'];
                    $order_data['email'] = empty($customer_info['email']) ? 'null' : $customer_info['email'];
                    $order_data['telephone'] = empty($customer_info['telephone']) ? 'null' : $customer_info['telephone'];
                } elseif (isset($this->session->data['guest'])) {
                    $order_data['firstname'] = empty($this->session->data['guest']['firstname']) ? 'null' : $this->session->data['guest']['firstname'];
                    $order_data['email'] = empty($this->session->data['guest']['email']) ? 'null' : $this->session->data['guest']['email'];
                    $order_data['telephone'] = empty($this->session->data['guest']['telephone']) ? 'null' : $this->session->data['guest']['telephone'];
                }
                $order_data['address'] = $this->model_extension_module_trade_import->get_order_address($this->session->data['order_id']);
                $order_data['address'] = empty($order_data['address']) ? 'null' : $order_data['address'];
                $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
                $order_data['store_id'] = $this->config->get('config_store_id');
                if ($order_data['store_id']) {
                    $order_data['store_url'] = parse_url($this->config->get('config_url'), PHP_URL_HOST);
                } else {
                    $order_data['store_url'] = parse_url(HTTP_SERVER, PHP_URL_HOST);
                }

                $data = array();
                $data['orders'] = array();
                $data['orders'][] = array(
                    'contractor'    => array(
                        'phone'     => $order_data['telephone'],
                        'email'     => $order_data['email'],
                        'name'      => $order_data['firstname'],
                        'address'   => $order_data['address'],
                    ),
                    'date'          => $date,
                    'description'   => $order_data['store_url'] . " - " . $order_data['invoice_prefix'] . "-" . $this->session->data['order_id'],
                );
                $data['orders'][0]['goods'] = array();
                foreach ($product_data as $product) {
                    $characteristic_uuid = $characteristic_price = $characterictic_discount = NULL;
                    foreach ($product['option'] as $option) {
                        if ($option['option_id'] == $this->model_extension_module_trade_import->get_optionid_by_code($this->model_extension_module_trade_import->get_product_code_by_id($product['product_id']))) {
                            $characteristic_uuid = $this->model_extension_module_trade_import->get_option_value_code_by_id($option['option_value_id']);
                            $characteristic_price = $option['price_old'];
                            $characterictic_discount = $option['price_old'] - $option['price'];
                            break;
                        }
                    }

                    $data['orders'][0]['goods'][] = array(
                        'nomenclature_uuid'     => $this->model_extension_module_trade_import->get_product_code_by_id($product['product_id']),
                        'characteristic_uuid'   => isset($characteristic_uuid) ? $characteristic_uuid : NULL,
                        'shipment_uuid'         => NULL,
                        'quantity'              => (double)$product['quantity'],
                        'price'                 => isset($characteristic_price) ? (double)$characteristic_price : (double)$product['price_old'],
                        'discount'              => isset($characterictic_discount) ? (double)$characterictic_discount : (double) ($product['price_old'] - $product['price']),
                        'total'                 => (double)$product['total']
                    );
                }
                if (!file_exists("trade_import")) {
                    mkdir("trade_import", 0777, true);
                }
                $order_url = $this->config->get('module_trade_import_order_address');
                $url = $this->config->get('module_trade_import_code');
                $token = $this->config->get('module_trade_import_token');
                $old_api_token = $this->config->get('module_trade_import_order_token');
                if (!$this->config->get('module_trade_import_enable_old_api')) {
                    $data_string = json_encode(array('token' => $token));
                    $ch = curl_init();
                    $header = array();
                    $header[] = "Content-Type: application/json";
                    $header[] = "UUID: " . $token;
                    $header[] = "Timestamp: " . $date;
                    $header[] = "Authorization: " . hash("sha512", $token . $time);
                    $header[] = "Content-Length: " . strlen($data_string);
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $response = curl_exec($ch);
                    if ($response === false) {
                        echo 'Curl error: ', curl_error($ch), "\n";
                        file_put_contents("trade_import/error_" . $date . ".log", curl_error($ch) . "\n" . $response);
                        $this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], 'ERROR: ' . $date . ".log");
                        curl_close($ch);
                        return 0;
                    } else {
                        $response_decoded = json_decode($response, true);
                        $access_token = $response_decoded['access_token'];
                        curl_close($ch);
                        $data_string = json_encode($data);
                        $ch = curl_init();
                        $header = array();
                        $header[] = "Content-Type: application/json";
                        $header[] = "Authorization: Bearer " . $access_token;
                        curl_setopt($ch, CURLOPT_URL, $order_url);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $response = curl_exec($ch);
                        curl_close($ch);
                    }
                } else {
                    $data_string = json_encode($data);
                    $ch = curl_init();
                    $header = array();
                    $header[] = "Content-Type: application/json";
                    $header[] = "UUID: " . $old_api_token;
                    $header[] = "Timestamp: " . $date;
                    $header[] = "Authorization: " . hash("sha512", $old_api_token . $time);
                    $header[] = "Content-Length: " . strlen($data_string);
                    curl_setopt($ch, CURLOPT_URL, $order_url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $response = curl_exec($ch);
                    curl_close($ch);
                }
                if (json_decode($response) !== NULL) {
                    $this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], $response);
                } else {
                    $this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], 'ERROR: ' . $date . ".log");
                    file_put_contents("trade_import/error_" . $date . ".log", $response);
                }
            }

            $data['metrika']["actionField"]['id'] = $this->config->get('config_invoice_prefix') . "-" . $this->session->data['order_id'];
            $data['metrika']['products'] = array();
            $this->load->model('catalog/product');
            foreach ($product_data as $product) {
                $category = $this->model_catalog_product->getCategoriesPath($product['product_id'], 5);
                if (!empty($product['option'])) {
                    foreach ($product['option'] as $option) {
                        $data['metrika']['products'][] = array(
                            "id" => str_replace('"', "", $product['model']),
                            "name" => str_replace('"', "", $product['name']),
                            "price" => $option['price'],
                            "category" => str_replace('"', "", $category),
                            "quantity" => $product['quantity'],
                            "variant" => str_replace('"', "", $option['name'])
                        );
                    }
                } else {
                    $data['metrika']['products'][] = array(
                        "id" => str_replace('"', "", $product['model']),
                        "name" => str_replace('"', "", $product['name']),
                        "price" => $product['price'],
                        "category" => str_replace('"', "", $category),
                        "quantity" => $product['quantity']
                    );
                }
            }
        }
        $this->cart->clear();
        unset($this->session->data['shipping_method']);
        unset($this->session->data['shipping_methods']);
        unset($this->session->data['payment_method']);
        unset($this->session->data['payment_methods']);
        $customer_group_id = $this->session->data['guest']['customer_group_id'];
        unset($this->session->data['guest']);
        $this->session->data['guest']['customer_group_id'] = $customer_group_id;
        unset($this->session->data['comment']);
        unset($this->session->data['order_id']);
        unset($this->session->data['coupon']);
        unset($this->session->data['reward']);
        unset($this->session->data['voucher']);
        unset($this->session->data['vouchers']);
        unset($this->session->data['totals']);
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        return $this->response->setOutput($this->load->view('extension/payment/tinkoff_success', $data));
    }
}
